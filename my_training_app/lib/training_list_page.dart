import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_training_app/training_model.dart';
import 'package:my_training_app/util/mta_theme.dart';
import 'package:my_training_app/util/verticaltab_util.dart';

class TrainingListPage extends StatefulWidget {
  const TrainingListPage({Key? key}) : super(key: key);


  @override
  State<TrainingListPage> createState() => _TrainingListPageState();
}

class _TrainingListPageState extends State<TrainingListPage> {
  final bool _pinned = true;
  late List<TrainingModel> trainingList =[];
  late List<ContentTileModel> locationList =[];
  late List<ContentTileModel> trainingNameList=[];
  late List<ContentTileModel> trainerList=[];

  @override
  void initState() {
    _readJsonData();
    super.initState();
  }

  Future<List<TrainingModel>> _readJsonData() async {
    final  jsonData =  await rootBundle.loadString('jsonfile/datafile.json');
    final list = json.decode(jsonData) as List<dynamic>;
     return List<TrainingModel>.from(list.map((e) => TrainingModel.fromJson(e)).toList());
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      endDrawer: const Drawer(),
      body: FutureBuilder(
          future: _readJsonData(),
          builder: (context, data) {
            if (data.hasError) {
              //in case if error found
              return Center(child: Text("${data.error}"));
            } else if (data.hasData) {
              trainingList = data.data as List<TrainingModel>;
              trainerList.clear();
              trainingNameList.clear();
              locationList.clear();
              for (var element in trainingList) {
                trainerList.add(ContentTileModel(element.trainer??"",  false));
                trainingNameList.add(ContentTileModel(element.name??"",  false));
                locationList.add(ContentTileModel( element.location??"", false));
              }

              return CustomScrollView(
                slivers: <Widget>[
                  SliverAppBar(
                    pinned: _pinned,
                    title: const Text('Trainings',
                      style: TextStyle(fontWeight: FontWeight.bold,
                        fontSize: 30.0,
                        color: Colors.white,),),
                    expandedHeight: 300.0,
                    flexibleSpace: FlexibleSpaceBar(
                      background: Stack(
                        alignment: Alignment.bottomCenter,
                        children: <Widget>[
                          Container(
                            height: 200.0, color: MTATheme().primaryColor,
                            alignment: Alignment.topLeft,
                            child: const Padding(
                              padding: EdgeInsets.only(left: 30.0
                              ),
                              child: Text('Highlights',
                                style: TextStyle(fontWeight: FontWeight.bold,
                                  fontSize: 20.0, color: Colors.white,),),
                            ),),
                          Container(
                            height: 100.0, color: MTATheme().secondaryColor,),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 30.0),
                                child: CarouselSlider(
                                  options: CarouselOptions(
                                    height: 140.0,
                                    enlargeCenterPage: true,
                                    aspectRatio: 16 / 9,
                                    autoPlayCurve: Curves.fastOutSlowIn,
                                    enableInfiniteScroll: true,
                                    viewportFraction: 0.8,),
                                  items: [1, 2, 3, 4, 5].map((i) {
                                    return Builder(
                                      builder: (BuildContext context) {
                                        return highlightRowWidget(i);
                                      },
                                    );
                                  }).toList(),
                                ),
                              )
                          )
                        ],


                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          TextButton(onPressed: () {
                            filterView();
                          },
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder
                                      (side: const BorderSide(
                                        color: Colors.black26,
                                        width: 0.5,
                                        style: BorderStyle.solid
                                    ),
                                        borderRadius: BorderRadius.circular(
                                            5.0))),),
                              child: Row(
                                children: const [
                                  Icon(Icons.filter_list_alt,
                                    color: Colors.black26, size: 15.0,),
                                  Text("Filter",
                                    style: TextStyle(color: Colors.black26),)
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                        return trainingRowWidget(index, trainingList);
                      },
                      childCount: trainingList.length,
                    ),
                  ),

                ],
              );
            }
            else {
              // show circular progress while data is getting fetched from json file
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }

  filterView(){
    showModalBottomSheet(context: context,
        builder:  (BuildContext bc){
          return Column(
            children: [
              Material(
                elevation: 1.0,
                child: SizedBox(height: 50.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: const [
                          Expanded(
                            child: Text("Sort and Filters",style: TextStyle(
                                fontSize: 20.0,fontWeight: FontWeight.bold
                            ),),
                          ),
                          Icon(Icons.close,color: Colors.black26,)
                        ],
                      ),
                    )),
              ),
              Expanded(
                child: VerticalTabs(
                  selectedTabBackgroundColor: Colors.white,
                  tabsWidth: 150,
                  tabs: const <Tab>[
                    Tab(child: Text('Sort by',style: TextStyle(
                        fontSize: 16.0,fontWeight: FontWeight.bold
                    ),)),
                    Tab(child: Text('Location',style: TextStyle(
                        fontSize: 16.0,fontWeight: FontWeight.bold
                    ),)),
                    Tab(child: Text('Training Name',style: TextStyle(
                        fontSize: 16.0,fontWeight: FontWeight.bold
                    ),)),
                    Tab(child: Text('Trainer',style: TextStyle(
                        fontSize: 16.0,fontWeight: FontWeight.bold
                    ),))
                  ],
                  contents: <Widget>[
                    tabsContent(false,locationList),
                    tabsContent(true,locationList),
                    tabsContent(true,trainingNameList),
                    tabsContent(true,trainerList),
                  ],
                ),
              ),
            ],
          );
        }
    );
  }

  Widget tabsContent(bool needSearchFiled, List<ContentTileModel> content) {
    return Column(
      children: [
        Visibility(
          visible: needSearchFiled,
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              decoration: InputDecoration(
                hintText: "Search",
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 0.0),
                  ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 0.0),
                ),

              ),

            ),
          ),
        ),
        const SizedBox(
          height: 4.0,
        ),
        Expanded(
          child: Container(
              color: Colors.white,
              child: ListView.builder(
                  itemCount: content.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: const EdgeInsets.all(10),
                      child: CheckboxListTile(
                        controlAffinity: ListTileControlAffinity.leading,
                        title: Text(content[index].title),
                        value: content[index].isCheck,
                        onChanged: (bool? value) {
                          setState(() {
                            content[index].isCheck = value!;
                            trainingList = trainingList.where((element) => element.location == content[index].title).toList();
                          });
                        },
                      ),
                    );
                  })
          ),
        ),
      ],
    );
  }

  highlightRowWidget(int i){
    return Container(
        width: MediaQuery.of(context).size.width - 100.0,
        margin: const EdgeInsets.symmetric(horizontal: 5.0),
        decoration: const BoxDecoration(
            color: Colors.black38
        ),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(color: MTATheme().primaryColor,
              alignment: Alignment.topLeft,
              child: Image.network(
                'https://blog.hubspot.com/hubfs/image-file-extensions.jpg',
                color: const Color.fromRGBO(1, 250, 250, 0.5),
                colorBlendMode: BlendMode.modulate,
                height: 250,
                width: double.infinity,
                fit: BoxFit.cover,
              ),),
             Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:  [
                      const Text("Safe Scrum Master",style: TextStyle(color: Colors.white,
                          fontWeight: FontWeight.bold, fontSize: 20.0),),
                      const SizedBox(height: 5.0,),
                      const Text("West Des Moines, IA - 30 Oct - 31 Oct",
                        style: TextStyle(color: Colors.white, fontSize: 14.0),),
                      const SizedBox(height: 10.0,),
                      Row(
                        children:  [
                          Text("\$975",style: TextStyle(color: MTATheme().primaryColor,
                              fontSize: 14.0, decoration: TextDecoration.lineThrough),),
                          const SizedBox(width: 3.0,),
                          Text("\$875",style: TextStyle(color: MTATheme().primaryColor,
                              fontWeight: FontWeight.bold, fontSize: 20.0),),
                          const Expanded(
                            child:  Text("View Details \u{2192}",textAlign: TextAlign.end,
                              style: TextStyle(color: Colors.white, fontSize: 12.0),),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
            )
          ],



        )
    );
  }

  trainingRowWidget(int index, List<TrainingModel> trainingModel){
    return Card(
      margin: EdgeInsets.all(20.0),
      child: Container(
        color:  Colors.white ,
        height: 160.0,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${trainingModel[index].date}', style: const TextStyle(fontSize: 20.0,
                    fontWeight: FontWeight.bold),),
                    const SizedBox(height: 15,),
                    Text('${trainingModel[index].time}',style: const TextStyle(fontSize: 10.0,)),
                    const SizedBox(height: 40,),
                    Text('${trainingModel[index].venue}',
                      style: const TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold),),
                    Text('${trainingModel[index].location}',
                      style: const TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              const Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.only(left: 4.0,right: 4.0),
                  child: DottedLine(
                    dashLength: 4,
                    dashGapLength: 4,
                    lineThickness: 2,
                    dashColor: Colors.black26,
                    dashGapColor: Colors.white,
                    direction: Axis.vertical,
                    // lineLength: 150,
                  ),
                ),
              ),
              Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:  [
                    Text('${trainingModel[index].status}', style: const TextStyle(fontSize: 10.0,
                        fontWeight: FontWeight.bold,color: Colors.red),),

                   Text('${trainingModel[index].name}',
                      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    const SizedBox(height: 5,),
                    Row(
                      children: [
                        Stack(
                          alignment: Alignment.bottomRight,
                          children: [

                            const CircleAvatar(
                              radius: 22,
                              backgroundColor: Colors.black,
                              backgroundImage: NetworkImage('https://cdn.pixabay.com/photo/2019/10/10/18/51/smartphone-4540273_960_720.jpg'),

                            ),
                            Positioned(
                              right: 0.0,
                              bottom: 1.0,
                              child: Container(
                                  width: 20.0,
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                  padding: const EdgeInsets.all(0.0),
                                  child:  const Icon(Icons.local_grocery_store_outlined,size: 15.0,)
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 12.0
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:  [
                               const Text('Keynote Speaker', style: TextStyle(fontSize: 12.0,
                                  fontWeight: FontWeight.bold),),
                               const SizedBox(height: 5,),
                               Text('${trainingModel[index].trainer}',
                                 style: const TextStyle(fontSize: 12.0,),)
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(onPressed: (){}, child: const Text('Enrol Now',style: TextStyle(color: Colors.white),),
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(MTATheme().primaryColor),
                          fixedSize: MaterialStateProperty.all(const Size.fromWidth(100.0))),)
                      ],
                    )

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ContentTileModel {
  String title;
  bool isCheck;

  ContentTileModel( this.title,  this.isCheck);

}




