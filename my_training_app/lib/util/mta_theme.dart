import 'dart:ui';

import 'package:flutter/material.dart';

class MTATheme {

  /// Gold Color
  Color primaryColor = Color(hexToColor("#ed4853"));
  Color secondaryColor = Color(hexToColor("#FFFFFF"));
  Color headingTextColor =  Color(hexToColor("#CEA406"));

  Color textColor =  Color(hexToColor("#221F1F"));
  Color subTextColor =  Color(hexToColor("#353333"));
  Color boarderColor =  Color(hexToColor("#707070"));

  Color grayColor =  Color(hexToColor("#A7A7A7"));
  Color lightGold = Color(hexToColor('#f5edcd'));
  Color statusColor = Color(hexToColor('#FE7430'));

  Color redColor =  Color(hexToColor("#E61212"));
  Color greenColor =  Color(hexToColor("#12BF40"));
  Color blueColor =  Color(hexToColor("#3E93C4"));


  Color drawerBg =   Color(hexToColor("#DDDDDD"));

  Color buttonGreenTopColor =   Color(hexToColor("#61b539"));
  Color buttonGreenBottomColor =   Color(hexToColor("#60bd34"));

  Color buttonRedTopColor =   Color(hexToColor("#ff0035"));
  Color buttonRedBottomColor =   Color(hexToColor("#fc2a5d"));

  Color spGradientTopColor =   Color(hexToColor("#00b7eb"));
  Color spGradientBottomColor =   Color(hexToColor("#76bd56"));

  Color bgColor =  Colors.white;


  ThemeData get themeData {

    ColorScheme colorScheme = ColorScheme(
      brightness: Brightness.light,
      primary: primaryColor,
      primaryVariant: primaryColor,
      secondary: primaryColor,
      secondaryVariant: primaryColor,
      background: bgColor,
      surface: bgColor,
      error: Colors.red.shade400,

      onBackground: headingTextColor,
      onSurface: headingTextColor,
      onError: Colors.white,
      onPrimary: bgColor,
      onSecondary: bgColor,
    );

    var themeData = ThemeData(
        colorScheme: colorScheme,
        fontFamily: 'Poppins',
        visualDensity: VisualDensity.adaptivePlatformDensity,
    );
    return themeData;
  }

  static int hexToColor(String hexString, {String alphaChannel = 'FF'}) {
    return int.parse(hexString.replaceFirst('#', '0x$alphaChannel'));
  }
}