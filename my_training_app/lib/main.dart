import 'package:flutter/material.dart';
import 'package:my_training_app/util/mta_theme.dart';

import 'training_list_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Training App',
      debugShowCheckedModeBanner: false,
      theme: MTATheme().themeData,
      home: const TrainingListPage(),
    );
  }
}

