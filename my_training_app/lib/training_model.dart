class TrainingModel {
  String? name;
  String? rating;
  String? trainer;
  String? date;
  String? time;
  String? venue;
  String? location;
  String? status;

  TrainingModel(
      {this.name,
        this.rating,
        this.trainer,
        this.date,
        this.time,
        this.venue,
        this.location,
        this.status});

  TrainingModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    rating = json['rating'];
    trainer = json['trainer'];
    date = json['date'];
    time = json['time'];
    venue = json['venue'];
    location = json['location'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['trainer'] = this.trainer;
    data['date'] = this.date;
    data['time'] = this.time;
    data['venue'] = this.venue;
    data['location'] = this.location;
    data['status'] = this.status;
    return data;
  }
}
